window.onload = function(){
    url = "http://192.168.0.102:8080/category.json";
    let responseData = null;
    fetch(url).then(res => {
        var ul = document.getElementById("category");
        var options = [];
        res.json().then((data)=> {
            console.log(data);
            data['data'].forEach(row => {
                var li = document.createElement('option');
                li.setAttribute('id',row.name);
                li.appendChild(document.createTextNode(row.id + ". " + row.name));
                ul.appendChild(li);                 
            });
        });
    }).catch(err =>  {
        console.log(err);
    });
}

